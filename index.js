// natural and simple

// Node.js -> will provide us with a runtime environment (RTE) that will allow us to execute our program

// RTE - (Runtime environment) - is the environment/system in which a program can be executed.

// TASK: Let's create a standard server setup using plain NodeJS

// identify and prepare the components/ingredients needed in order to execute the task.
	// http (hypertext transfer protocol)
	// http - is a built in module of NodeJS that will allow us to establish a connection and allow us to transfer data over the HTTP.

	//use a function called require() in order to acquire the desired module. repackage the module and store it inside a new variable.
let http = require('http');

	// http will provide us with all the components needed to establish a server.

	// createServer() -> will allow us to create an HTTP server.

	// provide/instruct the serve what to do.
		// sample - print a simple message

		// Identify and describe a location where the connection will happen, in order to provide a proper medium for both parties, and bind the connecion to the desired port number.
let port = 3000;

	// listen () -> bind and listen to a specific port whenever its being accessed by your computer.

	// identify a point where we want to terminate the transmission using the end() function

http.createServer(function (request, response){
	response.write('Hello Batch 145, Welcome to Backend!');
	response.end(); /*terminate the transmission*/
}).listen(port);

	// create a response in the terminal to confirm if a connection has been successfully established 
		// 1. we can give a confirmation to the client that a connection is working.
		// 2. we can specify what address or location the connection was created.
		// we will use template literals to better utilize placeholders
console.log(`Server is running with nodemon on port: ${port}`);

// What can we do for us to be able to automatically hotfix the system without restarting the server?

	// 1. initialize an npm into your local project
		// syntax: npm init
		// shortcut syntax: npm init -y (yes)

		// package.json -> "the heart of any Node Project". It records all the important metadata about the project, including (libraries, packages, functions) that makes up the system

	// 2. Install 'nodemon' into our project
		// PRACTICE SKILLS:
			// install:
				// npm install <dependencies>
				// uninstall:
					// npm uninstall dependency
					// npm un <dependency>
				// installing multiple dependencies
					//npm i <list of dependencies> 
						// express, nodemon

	// 3. run your app using the nodemon utility engine.
		// note: make sure that nodemon is recognized by your file system structure
		// npm install -g nodemon (global)
			// -> the library is being installed "globally" into your machine's file system so that the program wwill be recognizable across your file system.
			// * YOU ONLY HAVE TO EXECUTE THIS COMMAND ONCE
			// nodemon app.js
			// nodemon utility -> is a CLI utility tool, it's task is to wrap your node js app and watch for any changes in the file system and automatically restarts/hotfic 


			//create your personalized script to execute the app using the nodemon utility.
				 //register a new command to start the app.

				  // start -> common execute script for node apps

				  // others -> unusual that's why run command is added


	// [SEGWAY] Register Sublime Text 3 as part of your environment variables.

		//1. Locate in your PC where Sublime Text 3 is installed.
		//2. Copy the path where sublime text 3 is installed
			// C:\Program Files\Sublime Text
		//3. Locate the env variables in your system

